package fr.lille.univ.car.tp1.control;

public interface ICommand {
    FTPResponse execute(String[] arguments);
    boolean isFormatCorrect(String[] arguments);
}
