package fr.lille.univ.car.tp1.control.commands;
import fr.lille.univ.car.tp1.control.FTPResponse;
import fr.lille.univ.car.tp1.control.ICommand;
import fr.lille.univ.car.tp1.control.Processor;
import fr.lille.univ.car.tp1.utils.Settings;

public class UserCommand implements ICommand {
    private Processor processor;
    public UserCommand (Processor processor) {
        this.processor = processor;
    }

    public FTPResponse execute(String[] arguments) {
        if (!isFormatCorrect(arguments)) {
            return new FTPResponse(501, "Syntax error in parameters or arguments.");
        }

        String username = arguments[1];
        if (username.equals("remy")) {
            this.processor.state.setCurrentPath(Settings.DEFAULT_PATH(username));
            return new FTPResponse(331, "Username ok, need password");
        }
        else
            return new FTPResponse(530, "Not logged in");
    }

    @Override
    public boolean isFormatCorrect(String[] arguments) {
        return arguments.length == 2;
    }
}
