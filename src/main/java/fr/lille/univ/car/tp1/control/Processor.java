package fr.lille.univ.car.tp1.control;

import fr.lille.univ.car.tp1.control.commands.*;
import fr.lille.univ.car.tp1.control.commands.ListCommand;
import fr.lille.univ.car.tp1.utils.State;

import java.io.PrintStream;
import java.util.HashMap;

public class Processor {
    private HashMap<String, ICommand> commands = new HashMap<>();
    public State state = new State();
    private PrintStream out;

    public Processor () {
        this.out = null;
        this.registerAllCommands();
    }

    public Processor (PrintStream out) {
        this.out = out;
        this.registerAllCommands();
    }

    // TODO refactor to put "out" in state, and transmit state to all commands
    private void registerAllCommands () {
        this.registerCommand("USER", new UserCommand(this));
        this.registerCommand("PASS", new PassCommand());
        this.registerCommand("PWD", new PwdCommand(this));
        this.registerCommand("PASV", new PasvCommand(this));
        this.registerCommand("LIST", new ListCommand(this));
        this.registerCommand("CWD", new CwdCommand(this));
    }

    private boolean hasCommand (String name) {
        return this.commands.containsKey(name);
    }

    private void registerCommand (String name, ICommand command) {
        if (!this.hasCommand(name))
            this.commands.put(name, command);
    }

    public FTPResponse processRequest (String req) {
        System.out.println("control: " + req);
        String[] words = req.split(" ");
        String commandName = words[0];

        if (this.hasCommand(commandName)) {
            return this.commands.get(commandName).execute(words);
        } else {
            return new FTPResponse(202, "Command not supported");
        }
    }

    public void printToControlSocket (String message) {
        this.out.println(message);
    }
}
