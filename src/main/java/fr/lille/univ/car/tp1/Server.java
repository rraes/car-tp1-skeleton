package fr.lille.univ.car.tp1;

import fr.lille.univ.car.tp1.utils.Settings;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static void main(String... args) throws IOException {
        int PORT = Settings.FTP_CONTROL_PORT;
        System.out.println("Now listening on port " + PORT + "!");
        ServerSocket serverSocket = new ServerSocket(PORT);

        while (true) {
            Socket clientSocket = serverSocket.accept();
            new Thread(new Client(clientSocket)).start();
        }
    }
}
