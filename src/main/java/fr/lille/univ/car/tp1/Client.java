package fr.lille.univ.car.tp1;

import fr.lille.univ.car.tp1.control.FTPResponse;
import fr.lille.univ.car.tp1.control.Processor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class Client implements Runnable {

    private BufferedReader in;
    private PrintStream out; // out.println(...)
    private Processor processor;

    Client(Socket socket) throws IOException {
        in = new BufferedReader(new InputStreamReader(
                socket.getInputStream()));
        out = new PrintStream(socket.getOutputStream());
        processor = new Processor(out);
    }

    @Override
    public void run() {
        System.out.println("listening to new client");

        // this message is MANDATORY => https://tools.ietf.org/html/rfc959#5.4
        out.println("220 ready to exchange!");
        String line = null;

        try {
            while ((line = in.readLine()) != null) {
                process(line);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        processor.state.closeSocket();
        System.out.println("client closed connection");
    }

    private void process(String request) {
        FTPResponse rep = processor.processRequest(request);
        if (rep != null)
            out.println(rep.toString());
    }
}
