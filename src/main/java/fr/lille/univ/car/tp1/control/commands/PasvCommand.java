package fr.lille.univ.car.tp1.control.commands;
import fr.lille.univ.car.tp1.control.FTPResponse;
import fr.lille.univ.car.tp1.control.ICommand;
import fr.lille.univ.car.tp1.control.Processor;
// import fr.lille.univ.car.tp1.data.DataConnection;

public class PasvCommand implements ICommand {
    private Processor processor;

    public PasvCommand (Processor processor) {
        this.processor = processor;
    }

    public FTPResponse execute(String[] arguments) {
        this.processor.state.openDataSocket();
        return new FTPResponse(227, "Entering Passive Mode (127,0,0,1,42,42)");
    }

    @Override
    public boolean isFormatCorrect(String[] arguments) {
        return false;
    }
}
