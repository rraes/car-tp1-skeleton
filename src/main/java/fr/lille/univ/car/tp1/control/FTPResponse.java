package fr.lille.univ.car.tp1.control;

public class FTPResponse {
    public final int status;
    public final String message;

    public FTPResponse(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public String toString () {
        return "" + this.status + " " + this.message;
    }
}
