package fr.lille.univ.car.tp1.control.commands;
import fr.lille.univ.car.tp1.control.FTPResponse;
import fr.lille.univ.car.tp1.control.ICommand;
import fr.lille.univ.car.tp1.control.Processor;

public class PwdCommand implements ICommand {
    private Processor processor;
    public PwdCommand (Processor processor) {
        this.processor = processor;
    }

    public FTPResponse execute(String[] arguments) {
        if (!isFormatCorrect(arguments)) {
            return new FTPResponse(501, "Syntax error in parameters or arguments.");
        }
        return new FTPResponse(257, this.processor.state.getCurrentPath());
    }

    @Override
    public boolean isFormatCorrect(String[] arguments) {
        return arguments.length == 1;
    }
}
