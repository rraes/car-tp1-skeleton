package fr.lille.univ.car.tp1.utils;

public class Settings {
    public static final int FTP_CONTROL_PORT = 1024;
    public static final int FTP_DATA_PORT = 10794;
    public static String DEFAULT_PATH (String username) {
        return "/home/" + username;
    }
}
