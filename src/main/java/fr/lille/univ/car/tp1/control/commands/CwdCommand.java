package fr.lille.univ.car.tp1.control.commands;
import fr.lille.univ.car.tp1.control.FTPResponse;
import fr.lille.univ.car.tp1.control.ICommand;
import fr.lille.univ.car.tp1.control.Processor;
// import fr.lille.univ.car.tp1.data.DataConnection;

public class CwdCommand implements ICommand {
    private Processor processor;

    public CwdCommand(Processor processor) {
        this.processor = processor;
    }

    public FTPResponse execute(String[] arguments) {
        if (!isFormatCorrect(arguments)) {
            return new FTPResponse(501, "Syntax error in parameters or arguments.");
        }

        String targetPath = arguments[1];
        if (this.processor.state.checkPath(targetPath)) {
            this.processor.state.setCurrentPath(targetPath);
            return new FTPResponse(200, "Working directory changed");
        } else {
            return new FTPResponse(431, "No such directory");
        }
    }

    @Override
    public boolean isFormatCorrect(String[] arguments) {
        return arguments.length == 2;
    }
}
