package fr.lille.univ.car.tp1.control.commands;

import fr.lille.univ.car.tp1.control.FTPResponse;
import fr.lille.univ.car.tp1.control.ICommand;
import fr.lille.univ.car.tp1.control.Processor;

import java.io.*;
import java.net.Socket;
import java.util.Arrays;

public class ListCommand implements ICommand {
    private Processor processor;

    public ListCommand (Processor processor) {
        this.processor = processor;
    }

    @Override
    public FTPResponse execute(String[] arguments) {
        Socket socket = this.processor.state.getDataSocket();
        PrintStream out = null;
        try {
            out = new PrintStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (out == null) {
            throw new NullPointerException();
        }

        if (!isFormatCorrect(arguments)) {
            return new FTPResponse(501, "Syntax error in parameters or arguments.");
        }

        String s;
        Process p;
        this.processor.printToControlSocket("150 Starting transfer");

        try {
            p = Runtime.getRuntime().exec("ls -l " + this.processor.state.getCurrentPath());
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(p.getInputStream()));
            while ((s = br.readLine()) != null)
                out.println(s);
            p.waitFor();
            p.destroy();
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.processor.printToControlSocket("226 directory successfully transmitted");
        try {
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean isFormatCorrect(String[] arguments) {
        return arguments.length == 1;
    }
}
