package fr.lille.univ.car.tp1.utils;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class State {
    private ServerSocket socket;
    private boolean socketOpened = false;
    private String homePath = null;
    private String currentPath;

    public State () {
        this.socket = null;
    }

    public void openDataSocket () {
        try {
            if (!socketOpened) {
                this.socketOpened = true;
                this.socket = new ServerSocket(Settings.FTP_DATA_PORT);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Socket getDataSocket () {
        try {
            return socket.accept();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void closeSocket () {
        try {
            this.socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getCurrentPath () {
        return this.currentPath;
    }
    public void setCurrentPath (String path) {
        this.currentPath = path;
        if (this.homePath == null) {
            this.homePath = path;
        }
    }
    public boolean checkPath (String path) {
        return path.contains(this.homePath);
    }
}
