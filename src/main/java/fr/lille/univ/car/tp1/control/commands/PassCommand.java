package fr.lille.univ.car.tp1.control.commands;
import fr.lille.univ.car.tp1.control.FTPResponse;
import fr.lille.univ.car.tp1.control.ICommand;

public class PassCommand implements ICommand {
    public FTPResponse execute(String[] arguments) {
        if (!isFormatCorrect(arguments)) {
            return new FTPResponse(501, "Syntax error in parameters or arguments.");
        }

        if (arguments[1].equals("test"))
            return new FTPResponse(230, "User is logged in");
        else
            return new FTPResponse(530, "Not logged in");
    }

    @Override
    public boolean isFormatCorrect(String[] arguments) {
        return (arguments.length == 2);
    }
}
