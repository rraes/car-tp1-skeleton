package fr.lille.univ.car.tp1.commands;

import fr.lille.univ.car.tp1.control.FTPResponse;
import fr.lille.univ.car.tp1.control.Processor;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PwdCommandTest {
    private Processor processor;

    @Before
    public void initProcessor () {
        processor = new Processor();
    }

    @Test
    public void testNoArguments() {
        processor.processRequest("USER remy");
        FTPResponse rep = processor.processRequest("PWD");
        Assert.assertEquals(257, rep.status);
        Assert.assertEquals("/home/remy", rep.message);
    }

    @Test
    public void testWrongArgumentsCount() {
        FTPResponse rep = processor.processRequest("PWD hello bonsoir");
        Assert.assertEquals(501, rep.status);
        Assert.assertEquals("Syntax error in parameters or arguments.", rep.message);
    }
}
