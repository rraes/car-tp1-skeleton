package fr.lille.univ.car.tp1;

import fr.lille.univ.car.tp1.control.FTPResponse;
import fr.lille.univ.car.tp1.control.Processor;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ProcessorTest {
    private Processor processor;

    @Before
    public void initProcessor () {
        processor = new Processor();
    }


    @Test
    public void testHasNotCommand() {
        FTPResponse rep = processor.processRequest("BONSOIR test");
        Assert.assertEquals(202, rep.status);
        Assert.assertEquals("Command not supported", rep.message);
    }

    @Test
    public void testHasUserCommand() {
        FTPResponse rep = processor.processRequest("USER hello");
        Assert.assertEquals(530, rep.status);
        Assert.assertEquals("Not logged in", rep.message);
    }
}
