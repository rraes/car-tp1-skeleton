package fr.lille.univ.car.tp1.commands;

import fr.lille.univ.car.tp1.control.FTPResponse;
import fr.lille.univ.car.tp1.control.Processor;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UserCommandTest {
    private Processor processor;

    @Before
    public void initProcessor () {
        processor = new Processor();
    }

    @Test
    public void testNoArguments() {
        FTPResponse rep = processor.processRequest("USER");
        Assert.assertEquals(501, rep.status);
        Assert.assertEquals("Syntax error in parameters or arguments.", rep.message);
    }

    @Test
    public void testWrongArgumentsCount() {
        FTPResponse rep = processor.processRequest("USER hello bonsoir");
        Assert.assertEquals(501, rep.status);
        Assert.assertEquals("Syntax error in parameters or arguments.", rep.message);
    }

    @Test
    public void testUnknownUserName() {
        FTPResponse rep = processor.processRequest("USER test");
        Assert.assertEquals(530, rep.status);
        Assert.assertEquals("Not logged in", rep.message);
    }

    @Test
    public void testKnownUserName() {
        FTPResponse rep = processor.processRequest("USER remy");
        Assert.assertEquals(331, rep.status);
        Assert.assertEquals("Username ok, need password", rep.message);
    }
}
