package fr.lille.univ.car.tp1.commands;

import fr.lille.univ.car.tp1.control.FTPResponse;
import fr.lille.univ.car.tp1.control.Processor;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PassCommandTest {
    private Processor processor;

    @Before
    public void initProcessor () {
        processor = new Processor();
    }

    @Test
    public void testNoArguments() {
        FTPResponse rep = processor.processRequest("PASS");
        Assert.assertEquals(501, rep.status);
        Assert.assertEquals("Syntax error in parameters or arguments.", rep.message);
    }

    @Test
    public void testWrongArgumentsCount() {
        FTPResponse rep = processor.processRequest("PASS hello bonsoir");
        Assert.assertEquals(501, rep.status);
        Assert.assertEquals("Syntax error in parameters or arguments.", rep.message);
    }

    @Test
    public void testWrongPassword() {
        FTPResponse rep = processor.processRequest("PASS cc");
        Assert.assertEquals(530, rep.status);
        Assert.assertEquals("Not logged in", rep.message);
    }

    @Test
    public void testKnownUserName() {
        FTPResponse rep = processor.processRequest("PASS test");
        Assert.assertEquals(230, rep.status);
        Assert.assertEquals("User is logged in", rep.message);
    }
}
