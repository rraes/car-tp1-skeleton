package fr.lille.univ.car.tp1.utils;

import org.junit.Assert;
import org.junit.Test;

public class SettingsTest {
    @Test
    public void testFtpControlPort() {
        Assert.assertEquals(Settings.FTP_CONTROL_PORT, 1024);
    }

    @Test
    public void testFtpDataPort() {
        Assert.assertEquals(Settings.FTP_DATA_PORT, 10794);
    }
}
